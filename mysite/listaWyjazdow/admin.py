from django.contrib import admin
from .models import Wyjazd, Obiekthotelarski, Pokoj, Oferta

admin.site.register(Wyjazd)
admin.site.register(Obiekthotelarski)
admin.site.register(Pokoj)
admin.site.register(Oferta)