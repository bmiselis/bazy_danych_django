# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Adres(models.Model):
    adresid = models.AutoField(db_column='AdresID', primary_key=True)  # Field name made lowercase.
    ulicaid = models.ForeignKey('Ulica', models.DO_NOTHING, db_column='UlicaID', blank=True, null=True)  # Field name made lowercase.
    kraj = models.ForeignKey('Kontynentkraju', models.DO_NOTHING, db_column='kraj', blank=True, null=True)
    miasto = models.CharField(max_length=50, blank=True, null=True)
    kodpocztowy = models.CharField(db_column='kodPocztowy', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Adres'


class Kontynentkraju(models.Model):
    kraj = models.CharField(primary_key=True, max_length=30)
    kontynent = models.CharField(max_length=19, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KontynentKraju'


class Obiekthotelarski(models.Model):
    obiektid = models.AutoField(db_column='ObiektID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(max_length=10, blank=True, null=True)
    liczbagwiazdek = models.IntegerField(db_column='liczbaGwiazdek', blank=True, null=True)  # Field name made lowercase.
    mabasen = models.IntegerField(db_column='maBasen', blank=True, null=True)  # Field name made lowercase.
    masilownie = models.IntegerField(db_column='maSilownie', blank=True, null=True)  # Field name made lowercase.
    rodzajwyzywienia = models.CharField(db_column='rodzajWyzywienia', max_length=13, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ObiektHotelarski'


class Oferta(models.Model):
    ofertaid = models.AutoField(db_column='OfertaID', primary_key=True)  # Field name made lowercase.
    datadodania = models.DateField(db_column='dataDodania', blank=True, null=True)  # Field name made lowercase.
    czyaktywna = models.IntegerField(db_column='czyAktywna', blank=True, null=True)  # Field name made lowercase.
    opis = models.CharField(max_length=10000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Oferta'


class Pokoj(models.Model):
    pokojid = models.AutoField(db_column='PokojID', primary_key=True)  # Field name made lowercase.
    obiektid = models.ForeignKey(Obiekthotelarski, models.DO_NOTHING, db_column='ObiektID', blank=True, null=True)  # Field name made lowercase.
    liczbamiejsc = models.IntegerField(db_column='liczbaMiejsc', blank=True, null=True)  # Field name made lowercase.
    malazienke = models.IntegerField(db_column='maLazienke', blank=True, null=True)  # Field name made lowercase.
    maklimatyzacje = models.IntegerField(db_column='maKlimatyzacje', blank=True, null=True)  # Field name made lowercase.
    matelewizor = models.IntegerField(db_column='maTelewizor', blank=True, null=True)  # Field name made lowercase.
    maanekskuchenny = models.IntegerField(db_column='maAneksKuchenny', blank=True, null=True)  # Field name made lowercase.
    masejf = models.IntegerField(db_column='maSejf', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Pokoj'


class Ulica(models.Model):
    ulicaid = models.AutoField(db_column='UlicaID', primary_key=True)  # Field name made lowercase.
    nazwaulicy = models.CharField(db_column='nazwaUlicy', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numerdomu = models.CharField(db_column='numerDomu', max_length=5, blank=True, null=True)  # Field name made lowercase.
    numermieszkania = models.IntegerField(db_column='numerMieszkania', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Ulica'


class Wycieczkafakultatywna(models.Model):
    wycieczkaid = models.AutoField(db_column='WycieczkaID', primary_key=True)  # Field name made lowercase.
    wyjazdid = models.ForeignKey('Wyjazd', models.DO_NOTHING, db_column='WyjazdID', blank=True, null=True)  # Field name made lowercase.
    nazwa = models.CharField(max_length=100, blank=True, null=True)
    cena = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'WycieczkaFakultatywna'


class Wyjazd(models.Model):
    wyjazdid = models.AutoField(db_column='WyjazdID', primary_key=True)  # Field name made lowercase.
    ofertaid = models.ForeignKey(Oferta, models.DO_NOTHING, db_column='OfertaID', blank=True, null=True)  # Field name made lowercase.
    cena = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    datarozpoczecia = models.DateField(db_column='dataRozpoczecia', blank=True, null=True)  # Field name made lowercase.
    datazakonczenia = models.DateField(db_column='dataZakonczenia', blank=True, null=True)  # Field name made lowercase.
    liczbamiejsc = models.IntegerField(db_column='liczbaMiejsc', blank=True, null=True)  # Field name made lowercase.
    liczbapozostalychmiejsc = models.IntegerField(db_column='liczbaPozostalychMiejsc', blank=True, null=True)  # Field name made lowercase.
    rodzajtransportu = models.CharField(db_column='rodzajTransportu', max_length=7, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Wyjazd'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
