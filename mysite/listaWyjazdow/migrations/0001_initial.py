# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-08 15:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Wyjazd',
            fields=[
                ('wyjazdid', models.IntegerField(db_column='WyjazdID', primary_key=True, serialize=False)),
                ('cena', models.DecimalField(decimal_places=2, max_digits=5)),
                ('datarozpoczecia', models.DateField(db_column='dataRozpoczecia')),
                ('datazakonczenia', models.DateField(db_column='dataZakonczenia')),
                ('liczbamiejsc', models.IntegerField(db_column='liczbaMiejsc')),
                ('liczbapozostalychmiejsc', models.IntegerField(db_column='liczbaPozostalychMiejsc')),
                ('rodzajtransportu', models.CharField(db_column='rodzajTransportu', max_length=7)),
            ],
            options={
                'db_table': 'wyjazd',
                'managed': False,
            },
        ),
    ]
