from django.conf.urls import url
from . import views

urlpatterns = [
    #Strona glowna. Ma sie na niej pokazac zadany view
    url(r'^$', views.showWyjazdy, name='wyjazdDetail'),
    url(r'^oferta/(?P<pk>[0-9]+)/$', views.wyjazdDetail, name='wyjazdDetail'),
]