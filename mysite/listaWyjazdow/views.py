from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from .models import Wyjazd, Oferta


def showWyjazdy(request):
    #tworzę querrySet dla "Wyjazd" (lista obiektow encji "Wyjazd")
    # z odpowiednio posortowanych (tutaj po l. miejsc) wyjazdow
    wyjazdy = Wyjazd.objects.all().order_by('cena')
    #przekazuje querrySet 'wyjazdy' do odpowiedniego szablonu, ktory nazywam tak samo jak ten view
    return render(request, 'listaWyjazdow/wyjazdyTemplate.html', {'wyjazdy': wyjazdy})

def wyjazdDetail(request, pk):
    wyjazd = get_object_or_404(Wyjazd, pk=pk)
    return render(request, 'listaWyjazdow/wyjazdDetailTemplate.html', {'wyjazd': wyjazd})
