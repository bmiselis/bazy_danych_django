# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: biuro_podrozy
# Generation Time: 2017-05-08 14:54:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Adres
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Adres`;

CREATE TABLE `Adres` (
  `AdresID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `UlicaID` int(5) unsigned DEFAULT NULL,
  `kraj` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `miasto` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodPocztowy` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`AdresID`),
  KEY `UlicaID` (`UlicaID`),
  KEY `kraj` (`kraj`),
  CONSTRAINT `UlicaID` FOREIGN KEY (`UlicaID`) REFERENCES `Ulica` (`UlicaID`),
  CONSTRAINT `kraj` FOREIGN KEY (`kraj`) REFERENCES `KontynentKraju` (`kraj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table KontynentKraju
# ------------------------------------------------------------

DROP TABLE IF EXISTS `KontynentKraju`;

CREATE TABLE `KontynentKraju` (
  `kraj` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `kontynent` enum('EUROPA','AZJA','AMERYKA PÓŁNOCNA','AMERYKA POŁUDNIOWA','AUSTRALIA I OCEANIA','AFRYKA') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kraj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table Oferta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Oferta`;

CREATE TABLE `Oferta` (
  `OfertaID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `dataDodania` date DEFAULT NULL,
  `czyAktywna` bit(1) DEFAULT NULL,
  `opis` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`OfertaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table Ulica
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Ulica`;

CREATE TABLE `Ulica` (
  `UlicaID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nazwaUlicy` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numerDomu` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numerMieszkania` int(5) DEFAULT NULL,
  PRIMARY KEY (`UlicaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table WycieczkaFakultatywna
# ------------------------------------------------------------

DROP TABLE IF EXISTS `WycieczkaFakultatywna`;

CREATE TABLE `WycieczkaFakultatywna` (
  `WycieczkaID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `WyjazdID` int(5) unsigned DEFAULT NULL,
  `nazwa` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cena` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`WycieczkaID`),
  KEY `WyjazdID` (`WyjazdID`),
  CONSTRAINT `WyjazdID` FOREIGN KEY (`WyjazdID`) REFERENCES `Wyjazd` (`WyjazdID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table Wyjazd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Wyjazd`;

CREATE TABLE `Wyjazd` (
  `WyjazdID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `OfertaID` int(5) unsigned DEFAULT NULL,
  `cena` decimal(5,2) unsigned DEFAULT NULL,
  `dataRozpoczecia` date DEFAULT NULL,
  `dataZakonczenia` date DEFAULT NULL,
  `liczbaMiejsc` tinyint(2) unsigned DEFAULT NULL,
  `liczbaPozostalychMiejsc` tinyint(2) DEFAULT NULL,
  `rodzajTransportu` enum('SAMOLOT','AUTOKAR','KOLEJ') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`WyjazdID`),
  KEY `OfertaID` (`OfertaID`),
  CONSTRAINT `OfertaID` FOREIGN KEY (`OfertaID`) REFERENCES `Oferta` (`OfertaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
